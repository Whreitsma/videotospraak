#!/usr/bin/env python3
"""configuration program for reading the config file"""
import yaml
__author__ = "Wietse Reitsma"


class ParseConfig:

    def __init__(self, configloc):
        self.configfile_location = configloc
        self.loaded_config = self.initiate_config()

    def initiate_config(self):
        with open(self.configfile_location, 'r') as stream:
            config = yaml.safe_load(stream)
        return config


