#!/usr/bin/env python3
"""
feature extraction part of the program
it uses OpenSMILE to extract data from the audio throughput

"""
import os
import opensmile
import math
from multiprocessing import Pool
import pandas
from folder_handler import FolderHandler


__author__ = "Wietse Reitsma"


class Extractor:
    def __init__(self, file_folder, extension, feature_set, numb_of_threads, num_of_channels):
        """
        :param file_folder: Folder
        :param extension:
        :param feature_set:
        :param numb_of_threads: Amount of threads openSMILE can use for the feature extraction
        :param num_of_channels: The amount of audio channels in the file, can be set in the config file
        """
        self.file_folder = file_folder
        self.file_extension = extension
        folder_handler = FolderHandler(file_folder, extension)
        self.opened_data_dir, self.working_folder = folder_handler.create_dir_var()
        self.end_data_folder = self.working_folder + "/features"
        self.feature_set = feature_set
        self.channels = num_of_channels
        self.number_of_threads = numb_of_threads

    def run_opensmile(self, data):
        """
        creates an openSMILE instance and processes the given data
        :param data an list containing names of .wav files
        :returns output, generated csv file from openSMILE
        """
        smile = opensmile.Smile(
            feature_set=opensmile.FeatureSet.__getattr__(self.feature_set),
            feature_level=opensmile.FeatureLevel.Functionals,
            loglevel=4,
            num_channels=self.channels,
            verbose=True,
            logfile=self.end_data_folder + "/smile.log")
        output = smile.process_files(data)
        return output

    def multi_process_extract(self):
        """multiprocess operation for openSMILE, because I don't think the multi threadded functionality works"""

        number = len(self.opened_data_dir)/self.number_of_threads
        math.trunc(number)
        number = int(number)
        # splits the dataset into chunks equal to the number of cores given.
        data = [self.opened_data_dir[x:x+number] for x in range(0, len(self.opened_data_dir), number)]
        # creates the pool in which the opensmile instances will be run
        with Pool(self.number_of_threads) as pool:
            output = pool.map(self.run_opensmile, data)
        pool.join()
        return output

    def extract(self):
        """Runs openSMILE on the .wav files using any given config"""
        try:
            os.mkdir(self.end_data_folder)
        except FileExistsError:
            pass
        os.chdir(self.file_folder)

        if self.number_of_threads > 1:
            output = self.multi_process_extract()
        else:
            output = self.run_opensmile(self.opened_data_dir)
        if isinstance(output, list):
            output = pandas.concat(output)
        output.to_csv(self.end_data_folder + "/output_TRAILS.csv", sep=",")
        return
