#!/usr/bin/env python3
"""
simple class for handeling folders
"""
import os

__author__ = "Wietse Reitsma"


class FolderHandler:
    def __init__(self, data_folder, extension):
        """
        :param data_folder: path to the folder containing the dataset
        :param extension: the extension used in the dataset
        """
        self.data_folder = data_folder
        self.file_extension = extension

    def create_dir_var(self):
        """
        Creates a list of all files in the folder
        Returns:
            :return usable_files: a list containing all the files with the correct extension in the given datafolder
            :return working_folder: path to where all of the input and output folders will be

        """
        opened_folder = os.listdir(self.data_folder)
        usable_files = []
        for file in opened_folder:
            if file.endswith(self.file_extension):
                if self.file_extension == ".wav":
                    usable_files.append(file)
                else:
                    file_path = os.path.join(self.data_folder, file)
                    usable_files.append(os.path.abspath(file_path))
            else:
                print(f"File has not correct extension: file={file}; needs to be: {self.file_extension}")
        data_folder = os.path.abspath(self.data_folder)
        temp = data_folder.split("/")
        temp.remove(temp[-1])
        working_folder = "/".join(temp)
        return usable_files, working_folder

