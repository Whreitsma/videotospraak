#!/usr/bin/env python3
"""
Mainscript of the VideoToSpraak pipeline
"""
import sys
import spacy_tester
from file_converter import FileConverter
from configuration import ParseConfig
from opensmile_extractor import Extractor

__author__ = "Wietse Reitsma"


def main():
    config = ParseConfig("configuration/config.yaml")
    data_dir = config.loaded_config["fileEdit"]["dataDirectory"]
    old_ext = config.loaded_config["fileEdit"]["oldExtension"]
    new_ext = config.loaded_config["fileEdit"]["newExtension"]
    start = config.loaded_config["fileEdit"]["startPos"]
    end = config.loaded_config["fileEdit"]["endPos"]
    opensmile_ext = config.loaded_config["opensmile"]["opensmileExtension"]
    number_of_threads = config.loaded_config["opensmile"]["numberOfThreads"]
    feature_set = config.loaded_config["opensmile"]["featureSet"]
    channels = config.loaded_config["opensmile"]["numberOfChannels"]
    if old_ext == ".txt":
        spacy_tester.main()
    else:
        converter = FileConverter(data_dir, old_ext, new_ext, start, end, number_of_threads)
        converter.change_extension()
        extractor = Extractor(converter.extracted_audio_dir,
                              opensmile_ext, feature_set, number_of_threads, channels)
        extractor.extract()
    return 0


if __name__ == "__main__":
    sys.exit(main())

