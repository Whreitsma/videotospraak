#!/usr/bin/env python3
"""
This program converts all data from a given file into objects that can be used for feature extraction in openSMILE.
This program has been optimised for multicore performance.
"""
from shutil import copyfile
import os
from multiprocessing import Pool
import ffmpeg
from folder_handler import FolderHandler

__author__ = "Wietse Reitsma"


class FileConverter:
    def __init__(self, data_folder, old_ext, new_extension, start_time, end_time, num_threads):
        """
        Class responsible for converting video to correct format and extracting the audio stream
        :param data_folder: Path to the folder containing the data
        :param old_ext:
        :param new_extension:
        :param start_time: Time to start the audio extraction
        :param end_time: Time to stop the audio extraction
        :param num_threads: number of threads(cores) used for multiprocessing
        """
        self.data_folder_path = data_folder
        self.old_ext = old_ext
        self.working_folder = ""
        self.new_extension = new_extension
        self.starting_time = start_time
        self.ending_time = end_time
        self.extracted_audio_dir = ""
        folder_handler = FolderHandler(self.data_folder_path, old_ext)
        self.opened_data_dir, self.working_folder = folder_handler.create_dir_var()
        self.number_of_threads = num_threads
        self.files_raising_errors = []

    def process_file(self, file):
        """Function for changing a file extension used for multiprocessing pool"""
        filename, extension = os.path.splitext(file)
        filename = filename.split("/")
        filename = filename[-1]
        copyfile(file, self.data_folder_path + "/" + filename + self.new_extension)
        return

    def process_ffmpeg(self, file):
        """extracts audio with ffmpeg. used for multiprocessing pool"""
        if file.startswith("._"):
            pass
        else:
            filename, extension = os.path.splitext(file)
            try:
                (
                    ffmpeg
                        .input(self.data_folder_path + "/" + file)
                        .audio.filter('atrim', start=self.starting_time, end=self.ending_time)
                        .output(self.extracted_audio_dir + "/" + filename + ".wav")
                        .run(overwrite_output=True, capture_stderr=True)
                )
            except ffmpeg.Error as error:
                self.files_raising_errors.append(file)
                print(f"{file} gave an error: {error.stderr.decode('utf8')}")

    def change_extension(self):
        """Converts extension on a file to a new given extension"""
        # sets the new data folder to 'modified'
        self.data_folder_path = self.working_folder + "/modified"
        try:
            # if 'modified' doesn't exist jet, it will be created
            os.mkdir(self.data_folder_path)
        except FileExistsError:
            # if it already exists, it wil copy the new files to it and modify them
            if os.listdir(self.data_folder_path):
                return self.extract_audio()

        with Pool(self.number_of_threads) as pool:
            pool.map(self.process_file, self.opened_data_dir)
        pool.terminate()
        return self.extract_audio()

    def extract_audio(self):
        """extracts audio from video file, trims it to the correct size and saves it to another location"""
        self.extracted_audio_dir = self.working_folder + "/extracted"
        try:
            os.mkdir(self.extracted_audio_dir)
        except FileExistsError:
            pass
        if os.listdir(self.extracted_audio_dir):
            return
        with Pool(self.number_of_threads) as pool:
            pool.map(self.process_ffmpeg, os.listdir(self.data_folder_path))
        pool.terminate()
        return
