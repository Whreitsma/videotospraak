#VideoToSpraak

This program is used to convert video into audio and extracts features from it with the use of openSMILE.


## Prerequirements
The tools needed to make the program able to run are:

    - opensmile-python V1.0.1
    - Sound eXchange V14.4.2 
    - ffmpeg V4.3.1
    - python 3.8

It's also recommended to run In an virtual environment to ensure there will be no conflicts with other apps.
We will be using virtualenv for creating one.

##Installation
First, clone the project to a desired location:
```
git clone https://Whreitsma@bitbucket.org/Whreitsma/videotospraak.git
cd videotospraak
```
Then we create an virtual environment in it:
```
pip install virtualenv
virtualenv venv/
source venv/bin/activate
```
Now we install the required packages for this project:
```
pip3 install -r requirements.txt
```
or you can run setup.py(NOT TESTED YET! USE AT OWN RISK.):
```
python3 setup.py install
```
## Running for the first time
This program uses an config file to receive its parameters.
It is highly recommended to take a look at **config.yaml** and change the params to suit your data.

| Params      | Description          |recommended to edit |
| ------------- |:-------------:|:-------------:|
| dataDirectory    | Location of the dataset |Yes|
| oldExtension     | The old format of the dataset |Yes |
| newExtension | The new format for the dataset |Not really|
| startPos | The position for where you want to start the strip; in seconds |Yes|
| endPos | The position for where you want to end the strip; in seconds |Yes|
| opensmileExtension | The format for the input in openSMILE |No|
| numberOfThreads | The number of threads you want to run openSMILE on |Yes|
| featureSet  | The feature set used for extracting features from the audio   | Yes  |
| numberOfChannels  | The number of channels within the audio-file, it varies by file | Yes  |

If you want leave the config file as it is and use the standard params you only need to make a folder in the root of the
project named **_data_** and store your dataset there and make sure the datatype is of .mod variant.

If all is in order you only need to run main.py
```
python3 main.py
```


   